#!/usr/bin/python3

from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
from laumio import *
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
from threading import Thread
import requests


# configuration
DEBUG = True
my_hostname = "mpd.lan"
distanceCapteur = 0
temperature = 0

#laumios = [/*laumio1,laumio2,laumio3,laumio4,laumio5,laumio6,laumio7,laumio8,laumio9, laumio10, laumio11]

laumio1 = Laumio("Laumio_1D9486")
laumio2 = Laumio("Laumio_104A13")
laumio3 = Laumio("Laumio_0FBFBF")
laumio4 = Laumio("Laumio_104F03")
laumio5 = Laumio("Laumio_10508F")
laumio6 = Laumio("Laumio_10805F")
laumio7 = Laumio("Laumio_CD0522")
laumio8 = Laumio("Laumio_0FC168")
laumio9 = Laumio("Laumio_D454DB")
laumio10 = Laumio("Laumio_107DA8")
laumio11 = Laumio("Laumio_88813D")


zone1 = [laumio1,laumio2,laumio3]
zone2 = [laumio4,laumio5, laumio6, laumio7, laumio8, laumio9, laumio10]
zone3 = [ laumio11]

zones = [zone1, zone2, zone3]

#laumios = [laumio11]
laumios = [laumio1,laumio2,laumio3,laumio4,laumio5,laumio6,laumio7,laumio8,laumio9, laumio10, laumio11]

class Scenario:
    def __init__(self, nom):
        Thread.__init__(self)
        self.nom = nom
        self.lumiere = ""
        self.musique = ""
        self.zone = []
    def execute(self):
        print("================================" + self.lumiere)
        if self.lumiere == "zen":
            print("Scenario zen")
            for laumio in self.zone:
                print ("=>>")
                laumio.fillColor(0, 0, 0)
                laumio.fillRing(0, 255, 140, temperature)
            ambianceLumineuse["ambiance"] = "zen"
            ambianceLumineuse["intensite"] = "basse"
        elif self.lumiere == "travail":
            print("Scenario travail")
            for laumio in self.zone:
                print ("=>>")
                laumio.fillColor(255, 255, 255)
            ambianceLumineuse["ambiance"] = "travail"
            ambianceLumineuse["intensite"] = "forte"
        elif self.lumiere == "fun":
            print("Scenario fun")
            for laumio in self.zone:
                print ("=>>")
                laumio.rainbow() # fillColor(255, 255, 255)
            ambianceLumineuse["ambiance"] = "fun"
            ambianceLumineuse["intensite"] = "forte"
        elif self.lumiere == "nuit":
            print("Scenario nuit")
            for laumio in self.zone:
                print ("=>>")
                laumio.fillColor(0, 0, 0)
            ambianceLumineuse["ambiance"] = "nuit"
            ambianceLumineuse["intensite"] = "eteinte"

        if self.musique == "forte":
            print("Scenario fort")
            publish.single("music/control/setvol", "100", hostname=my_hostname, port=1883)
            publish.single("music/control/play", "", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "forte"
        elif self.musique == "moyenne":
            print("Scenario moyenne")
            publish.single("music/control/setvol", "90", hostname=my_hostname, port=1883)
            publish.single("music/control/play", "", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "moyenne"
        elif self.musique == "faible":
            print("Scenario faible")
            publish.single("music/control/setvol", "80", hostname=my_hostname, port=1883)
            publish.single("music/control/play", "", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "faible"
        elif self.musique == "eteinte":
            publish.single("music/control/setvol", "0", hostname=my_hostname, port=1883)
            publish.single("music/control/stop", "", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "eteinte"

# class Meteo(Thread):
#     """Thread chargé simplement d'afficher une lettre dans la console."""
#     def __init__(self):
#         Thread.__init__(self)
#
#     def run(self):
#         while True:
#             r = requests.get("http://api.openweathermap.org/data/2.5/weather?appid=2be91b9dbc560a4b0f83a4c5e8f9d604&q=Le%20Mans&units=metric")
#             data=r.json()
#             print(data)
#             sleep(20)
#
# meteo = Meteo()
#
# # Lancement des threads
# meteo.start()


class Mqtt(Thread):
    """Thread chargé simplement d'afficher une lettre dans la console."""
    def __init__(self, lettre):
        Thread.__init__(self)
        self.lettre = lettre

    def run(self):
        mqttc = mqtt.Client()

        mqttc.connect(my_hostname, 1883, 0)

        #mqttc.subscribe("capteur_bp/switch/1/state", 1)
        #mqttc.subscribe("capteur_bp/switch/2/state", 1)
        mqttc.subscribe("distance/value", 1)
        mqttc.subscribe("capteur_bp/binary_sensor/bp1/state", 1)
        mqttc.subscribe("capteur_bp/binary_sensor/bp2/state", 1)
        mqttc.subscribe("capteur_bp/binary_sensor/bp3/state", 1)
        mqttc.subscribe("capteur_bp/binary_sensor/bp4/state", 1)

        mqttc.subscribe("remote/1/state", 1)
        mqttc.subscribe("remote/2/state", 1)
        mqttc.subscribe("remote/3/state", 1)
        mqttc.subscribe("remote/4/state", 1)

        mqttc.subscribe("presence/state", 1)

        mqttc.on_log = on_log
        mqttc.on_message = on_message
        print("Demarrage....")
        mqttc.loop_forever()


# instantiate the app
app = Flask(__name__, static_folder = "./dist/static", template_folder = "./dist")

thread_1 = Mqtt("1")

# Lancement des threads
thread_1.start()

# laumio1 = Laumio("Laumio_1D9486")
# laumio2 = Laumio("Laumio_104A13")
# laumio3 = Laumio("Laumio_0FBFBF")
# laumio4 = Laumio("Laumio_104F03")
# laumio5 = Laumio("Laumio_10508F")
# laumio6 = Laumio("Laumio_10805F")
# laumio7 = Laumio("Laumio_CD0522")
# laumio8 = Laumio("Laumio_0FC168")
# laumio9 = Laumio("Laumio_D454DB")
# laumio10 = Laumio("Laumio_107DA8")
# laumio11 = Laumio("Laumio_88813D")

scenarios = [Scenario("proche"), Scenario("loin"), Scenario("bouton1"), Scenario("bouton2"), Scenario("bouton3"), Scenario("bouton4")]

chambre = [laumio1,laumio2,laumio3]
cuisine = [laumio4]
salon = [laumio5]

#laumios = [laumio11]

ambianceLumineuse = { 'ambiance': "zen", 'intensite': "moyenne"}
ambianceMusicale = { 'intensite': "moyenne"}

# enable CORS
CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/')
def index():
    return render_template("index.html")

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/zen', methods=['POST'])
def zen():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('zen')

        #post_data = request.get_json()
        for laumio in laumios:
            laumio.fillColor(0, 0, 0)
            laumio.fillRing(0, 255, 140, temperature)

        publish.single("music/control/setvol", "80", hostname=my_hostname, port=1883)
        publish.single("music/control/play", "", hostname=my_hostname, port=1883)
        ambianceLumineuse["ambiance"] = "zen"
        ambianceLumineuse["intensite"] = "basse"
        ambianceMusicale["intensite"] = "basse"

    return jsonify(response_object)

@app.route('/travail', methods=['POST'])
def travail():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('travail')
        #post_data = request.get_json()
        for laumio in laumios:
            laumio.fillColor(255, 255, 255)
        publish.single("music/control/setvol", "0", hostname=my_hostname, port=1883)
        publish.single("music/control/stop", "", hostname=my_hostname, port=1883)
        ambianceLumineuse["ambiance"] = "travail"
        ambianceLumineuse["intensite"] = "forte"
        ambianceMusicale["intensite"] = "eteinte"

    return jsonify(response_object)

@app.route('/fun', methods=['POST'])
def fun():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        #post_data = request.get_json()
        for laumio in laumios:
            laumio.rainbow() # fillColor(255, 255, 255)
        publish.single("music/control/setvol", "100", hostname=my_hostname, port=1883)
        publish.single("music/control/next", "", hostname=my_hostname, port=1883)
        publish.single("music/control/play", "", hostname=my_hostname, port=1883)
        ambianceLumineuse["ambiance"] = "fun"
        ambianceLumineuse["intensite"] = "forte"
        ambianceMusicale["intensite"] = "forte"

    return jsonify(response_object)

@app.route('/nuit', methods=['POST'])
def nuit():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('nuit')
        for laumio in laumios:
            laumio.fillColor(0, 0, 0)
        publish.single("music/control/setvol", "0", hostname=my_hostname, port=1883)
        publish.single("music/control/stop", "", hostname=my_hostname, port=1883)
        ambianceLumineuse["ambiance"] = "nuit"
        ambianceLumineuse["intensite"] = "eteinte"
        ambianceMusicale["intensite"] = "eteinte"

    return jsonify(response_object)

@app.route('/luminosite/plus', methods=['POST'])
def luminositePlus():
    global ambianceLumineuse
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('luminosite/plus ' + ambianceLumineuse["ambiance"] + ' '  + ambianceLumineuse["intensite"])

        if ambianceLumineuse["intensite"] == "forte":
            print ("Rien a faire")
        elif ambianceLumineuse["ambiance"] == "zen" and ambianceLumineuse["intensite"] == "moyenne":
            for laumio in laumios:
                laumio.fillRing(2, 255, 140, temperature)
                laumio.setPixelColor(9, 255, 140, temperature)
            ambianceLumineuse["intensite"] = "forte"
            print('Etat ' + ambianceLumineuse["ambiance"] + ' '  + ambianceLumineuse["intensite"])
        elif ambianceLumineuse["ambiance"] == "travail" and ambianceLumineuse["intensite"] == "moyenne":
            for laumio in laumios:
                laumio.fillRing(2, 255, 255, 255)
                laumio.setPixelColor(9, 255, 255, 255)
            ambianceLumineuse["intensite"] = "forte"
        elif ambianceLumineuse["ambiance"] == "zen" and ambianceLumineuse["intensite"] == "faible":
            for laumio in laumios:
                laumio.fillRing(1, 255, 140, temperature)
            ambianceLumineuse["intensite"] = "moyenne"
        elif ambianceLumineuse["ambiance"] == "travail" and ambianceLumineuse["intensite"] == "faible":
            for laumio in laumios:
                laumio.fillRing(1, 255,255, 255)
            ambianceLumineuse["intensite"] = "moyenne"

    return jsonify(response_object)

@app.route('/luminosite/moins', methods=['POST'])
def luminositeMoins():
    global ambianceLumineuse
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('luminosite/moins ' + ambianceLumineuse["ambiance"] + ' '  + ambianceLumineuse["intensite"])

        if ambianceLumineuse["intensite"] == "faible":
            print ("Rien a faire")
        elif ambianceLumineuse["ambiance"] == "zen" and ambianceLumineuse["intensite"] == "moyenne":
            for laumio in laumios:
                laumio.fillRing(1, 0, 0, 0)
            ambianceLumineuse["intensite"] = "faible"
        elif ambianceLumineuse["ambiance"] == "travail" and ambianceLumineuse["intensite"] == "moyenne":
            for laumio in laumios:
                laumio.fillRing(1, 0, 0, 0)
            ambianceLumineuse["intensite"] = "faible"
        elif ambianceLumineuse["ambiance"] == "zen" and ambianceLumineuse["intensite"] == "forte":
            for laumio in laumios:
                laumio.fillRing(2, 0, 0, 0)
                laumio.setPixelColor(9, 0, 0, 0)
            ambianceLumineuse["intensite"] = "moyenne"
        elif ambianceLumineuse["ambiance"] == "travail" and ambianceLumineuse["intensite"] == "forte":
            for laumio in laumios:
                laumio.fillRing(2, 0, 0, 0)
                laumio.setPixelColor(9, 0, 0, 0)
            ambianceLumineuse["intensite"] = "moyenne"

    return jsonify(response_object)

app.route('/music/control/previous', methods=['POST'])
def musicPrevious():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('/music/control/previous')

    publish.single("music/control/previous", "", hostname=my_hostname, port=1883)
    return jsonify(response_object)

app.route('/music/control/toggle', methods=['POST'])
def musicToggle():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('/music/control/toggle')

    publish.single("music/control/toggle", "", hostname=my_hostname, port=1883)
    return jsonify(response_object)

app.route('/music/control/next', methods=['POST'])
def musicNext():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('/music/control/next')

    publish.single("music/control/next", "", hostname=my_hostname, port=1883)
    return jsonify(response_object)

@app.route('/volume/plus', methods=['POST'])
def volumePlus():
    global ambianceMusicale
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('volume/plus '+ ambianceMusicale["intensite"])

        if ambianceMusicale["intensite"] == "forte":
            print ("Rien a faire")
        elif ambianceMusicale["intensite"] == "moyenne":
            publish.single("music/control/setvol", "100", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "forte"
        elif ambianceMusicale["intensite"] == "faible":
            publish.single("music/control/setvol", "90", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "moyenne"
        elif ambianceMusicale["intensite"] == "eteinte":
            publish.single("music/control/setvol", "80", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "faible"
    return jsonify(response_object)

@app.route('/volume/moins', methods=['POST'])
def volumeMoins():
    global ambianceMusicale
    response_object = {'status': 'success'}
    if request.method == 'POST':
        print('volume/moins ' + ambianceMusicale["intensite"])

        if ambianceMusicale["intensite"] == "eteinte":
            print ("Rien a faire")
        elif ambianceMusicale["intensite"] == "forte":
            publish.single("music/control/setvol", "90", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "moyenne"
        elif ambianceMusicale["intensite"] == "moyenne":
            publish.single("music/control/setvol", "80", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "moyenne"
        elif ambianceMusicale["intensite"] == "faible":
            publish.single("music/control/setvol", "0", hostname=my_hostname, port=1883)
            ambianceMusicale["intensite"] = "eteinte"

    return jsonify(response_object)

@app.route('/meteo', methods=['POST'])
def meteo():
    global temperature
    json = request.get_json(force=True)
    print (json["ville"])
    r = requests.get("http://api.openweathermap.org/data/2.5/weather?appid=2be91b9dbc560a4b0f83a4c5e8f9d604&q=" + json["ville"] + "&units=metric")
    # r = requests.get("http://api.openweathermap.org/data/2.5/weather?appid=2be91b9dbc560a4b0f83a4c5e8f9d604&q=Le%20Mans&units=metric")
    data=r.json()
    print (int(data["main"]["temp"]))
    temp = int(data["main"]["temp"])
    if temp < 10:
        temperature = 255
    else:
        temperature = 0

    print(data)
    response_object = {'status': 'success'}
    return jsonify(response_object)

@app.route('/scenario', methods=['POST'])
def scenario():
    print ("json" + str(jsonify(request.get_json(force=True))))
    json = request.get_json(force=True)
    print (json["evenement"])
    print (json["action"]["lumiere"])
    print (json["action"]["musique"])
    post_data = request.get_json()

    for scenario in scenarios:
        if scenario.nom == json["evenement"]:
            print ("trouve..")
            scenario.lumiere = json["action"]["lumiere"]
            scenario.musique = json["action"]["musique"]
            print (int(json["action"]["zone"]))
            if int(json["action"]["zone"]) == 1:
                print(">1")
                scenario.zone = zone1 # zones[ int(json["action"]["zone"]) - 1]
            elif int(json["action"]["zone"]) == 2:
                print(">2")
                scenario.zone = zone2
            else:
                print(">3")
                scenario.zone = zone3

            # print (len(scenario.zone[0]))
            # for s in scenario.zone:
            #     print ("===>")
            #     print (s)

    response_object = {'status': 'success'}
    return jsonify(response_object)


# MQTT function
def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))
    print(obj)

def getScenario(nom):
    for scenario in scenarios:
        if scenario.nom == nom:
            return scenario

#
# Reception d'un message
#
def on_message(mqttc, obj, msg):
    global distanceCapteur
    print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
    if msg.topic == "distance/value":
        distance = msg.payload.decode("utf-8")
        print ("distanceCapteur " + str(distance) + " " + str(distanceCapteur))

        if float(distance)*100 < 30 and distanceCapteur >= 30:
            print ("Changement proche...")
            getScenario("proche").execute()

        if float(distance)*100 > 70 and distanceCapteur < 70:
            print ("Changement loin...")
            getScenario("loin").execute()
        distanceCapteur = float(distance)*100

    if msg.topic.startswith("capteur_bp/binary_sensor/bp"):
        etat = msg.payload.decode("utf-8")
        if etat == "ON":
            if msg.topic.find("1") > 0:
                print("Scenario1")
                getScenario("bouton1").execute()
            elif msg.topic.find("2") > 0:
                print("Scenario2")
                getScenario("bouton2").execute()
            elif msg.topic.find("3") > 0:
                print("Scenario3")
                getScenario("bouton4").execute()
            elif msg.topic.find("4") > 0:
                print("Scenario4")
                getScenario("bouton4").execute()

    # remote
    if msg.topic.startswith("remote/"):
        etat = msg.payload.decode("utf-8")
        if etat == "ON":
            if msg.topic.find("1") > 0:
                print("Scenario1")
                getScenario("bouton1").execute()
            elif msg.topic.find("2") > 0:
                print("Scenario2")
                getScenario("bouton2").execute()
            elif msg.topic.find("3") > 0:
                print("Scenario3")
                getScenario("bouton4").execute()
            elif msg.topic.find("4") > 0:
                print("Scenario4")
                getScenario("bouton4").execute()

    # Presence personne
    if msg.topic == "presence/state":
        etat = msg.payload.decode("utf-8")
        if etat == "ON":
            for laumio in laumios:
                laumio.fillColor(255,0,0)
        else:
            for laumio in laumios:
                laumio.wipeOut()

def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))

def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)

print("...")
if __name__ == '__main__':
    print("run")
    app.run()
