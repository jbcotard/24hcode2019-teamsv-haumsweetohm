#!/usr/bin/python3

import json
import logging
import os
# import pygame
import smtplib
import sys
import time
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt


# MQTT function
def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))
    print(obj)

#
# Reception d'un message
#
def on_message(mqttc, obj, msg):
    print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

#publish.single("laumio/Laumio_CD0522/json", "{  'command': 'animate_rainbow' }", hostname="mpd.lan",  port=1883)

# def on_message_state1(mqttc, obj, msg):
#     print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

# def on_message_state2(mqttc, obj, msg):
#     print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

if __name__ == '__main__':
    mqttc = mqtt.Client()

    mqttc.connect("localhost", 1883, 0)

    mqttc.subscribe("capteur_bp/#", 1)
    mqttc.subscribe("laumio/#", 1)
    mqttc.subscribe("remote/#", 1)
    mqttc.subscribe("presence/#", 1)
    mqttc.subscribe("distance/#", 1)
    mqttc.subscribe("atmosphere/#", 1)
    mqttc.subscribe("music/#", 1)
    mqttc.on_message = on_message

    mqttc.loop_forever()
print("End")
