export default {
  pictures: [
    {
      'id': 0,
      'url': 'https://d16f0eyt82qpj3.cloudfront.net/wordpress/wp-content/uploads/2017/09/10235935/vie-nocturne.jpg',
      'comment': 'Ambiance Nuit',
      'info': 'nuit'
    },
    {
      'id': 1,
      'url': 'https://www.lacellesaintcloud.fr/wp-content/uploads/2017/12/soiree-zen-piscine-corneille-690x390-690x350.jpg',
      'comment': 'Ambiance ZEN',
      'info': 'zen'
    },
    {
      'id': 2,
      'url': 'https://www.weka.fr/actualite/wp-content/uploads/2018/02/conditions-de-travail-fonction-publique-640x312.jpg',
      'comment': 'Ambiance travail',
      'info': 'travail'
    },
    {
      'id': 3,
      'url': 'https://archive.org/download/dark-music-wallpaper/dark-music-wallpaper.jpg',
      'comment': 'Ambiance FUN',
      'info': 'fun'
    }
  ],
  villes: [
    {
      'id': 0,
      'url': 'https://www.telegraph.co.uk/content/dam/Travel/2018/August/sydney.jpg',
      'comment': 'Sydney',
      'info': 'Sydney'
    },
    {
      'id': 1,
      'url': 'https://www.sensationsdumonde.com/img/destination/87/skyline_nyc__empire_state_building__shutterstock_2018_768a.jpg',
      'comment': 'New York',
      'info': 'New York'
    },
    {
      'id': 2,
      'url': 'https://media.routard.com/image/15/9/moscou-home-fiche.1466159.w740.jpg',
      'comment': 'Moscou',
      'info': 'Moscou'
    },
    {
      'id': 3,
      'url': 'https://www.movehub.fr/wp-content/uploads/2017/05/LeMans.png',
      'comment': 'Le Mans',
      'info': 'Le Mans'
    }
  ]
}
