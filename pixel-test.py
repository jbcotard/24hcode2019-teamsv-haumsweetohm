#!/usr/bin/python3
import time
import sys
from laumio import *

# publish.single("laumio/Laumio_104F03/json", "{  'command': 'animate_rainbow' }", hostname="mpd.lan", port=1883)


laumio1 = Laumio("Laumio_1D9486")
laumio3 = Laumio("Laumio_0FBFBF")
laumio4 = Laumio("Laumio_104F03")
laumio6 = Laumio("Laumio_10805F")
laumio7 = Laumio("Laumio_CD0522")
laumio8 = Laumio("Laumio_0FC168")
laumio9 = Laumio("Laumio_D454DB")
laumio10 = Laumio("Laumio_107DA8")
laumio11 = Laumio("Laumio_88813D")



# Tout éteindre

#laumio/all/fill 0 0 0

laumio11.wipeOut()

#Traitement pixel par pixel de la dernière boule : une petite
#lumière bleue qui se balade
#[11]
sleep(2)
#laumio/88813D/set_pixel 0 0 0 255
laumio11.setPixelColor(0, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 0 0 0 0
#laumio/88813D/set_pixel 1 0 0 255
laumio11.setPixelColor(0, 0, 0, 0)
laumio11.setPixelColor(1, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 1 0 0 0
#laumio/88813D/set_pixel 2 0 0 255
laumio11.setPixelColor(1, 0, 0, 0)
laumio11.setPixelColor(2, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 2 0 0 0
#laumio/88813D/set_pixel 3 0 0 255
laumio11.setPixelColor(2, 0, 0, 0)
laumio11.setPixelColor(3, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 3 0 0 0
#laumio/88813D/set_pixel 4 0 0 255
laumio11.setPixelColor(3, 0, 0, 0)
laumio11.setPixelColor(4, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 4 0 0 0
#laumio/88813D/set_pixel 5 0 0 255
laumio11.setPixelColor(4, 0, 0, 0)
laumio11.setPixelColor(5, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 5 0 0 0
#laumio/88813D/set_pixel 6 0 0 255
laumio11.setPixelColor(5, 0, 0, 0)
laumio11.setPixelColor(6, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 6 0 0 0
#laumio/88813D/set_pixel 7 0 0 255
laumio11.setPixelColor(6, 0, 0, 0)
laumio11.setPixelColor(7, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 7 0 0 0
#laumio/88813D/set_pixel 8 0 0 255
laumio11.setPixelColor(7, 0, 0, 0)
laumio11.setPixelColor(8, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 8 0 0 0
#laumio/88813D/set_pixel 10 0 0 255
laumio11.setPixelColor(8, 0, 0, 0)
laumio11.setPixelColor(10, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 10 0 0 0
#laumio/88813D/set_pixel 11 0 0 255
laumio11.setPixelColor(10, 0, 0, 0)
laumio11.setPixelColor(11, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 11 0 0 0
#laumio/88813D/set_pixel 12 0 0 255
laumio11.setPixelColor(11, 0, 0, 0)
laumio11.setPixelColor(12, 0, 0, 255)
sleep(1)
#laumio/88813D/set_pixel 12 0 0 0
#laumio/88813D/set_pixel 9 0 0 255
laumio11.setPixelColor(12, 0, 0, 0)
laumio11.setPixelColor(9, 0, 0, 255)
#sleep(1)
#laumio/88813D/set_pixel 9 0 0 0
#laumio11.setPixelColor(0, 0, 0, 0)

