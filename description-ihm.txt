Deux interfaces à mettre en place, la première étant une IHM de commande temps réel de l'environnement, et la deuxième une interface de définition des scénarios (cf. interface ALEXA).
Les interfaces seront accessibles via un navigateur sur un PC ou sur un smartphone.

Interface de commande temps réel
================================

Choix de l'ambiance
-------------------
Zen
Travail
Fun
Nuit

Commandes
---------
Plus lumineux | Moins lumineux
Chant précédent | Musique On / Off | Chant suivant
Volume plus fort | Volume moins fort

Détail des ambiances
====================
Zen
---
fillColor(0, 0, 0)
fillRing(0, 218, 251, 102)
AmbianceLumineuse = zen basse
music/control/setvol 60
music/control/play
AmbianceMusicale = basse

Travail
-------
fillColor(255, 255, 255)
AmbianceLumineuse = travail forte
music/control/setvol 0
music/control/stop
AmbianceMusicale = éteinte

Fun
---
rainbow()
AmbianceLumineuse = fun
music/control/setvol 100
music/control/next
music/control/play
AmbianceMusicale = forte

Nuit
---
fillColor(0, 0, 0)
AmbianceLumineuse = éteinte
music/control/setvol 0
music/control/stop
AmbianceMusicale = éteinte

Détail des Commandes
====================
Plus lumineux
-------------
Si AmbianceLumineuse = zen forte, travail forte, fun forte
    Alors
        rien
Si AmbianceLumineuse = zen moyenne
    Alors
        fillRing(2, 218, 251, 102)
        setPixelCOlor(9, 218, 251, 102)
        AmbianceLumineuse = zen forte
Si AmbianceLumineuse = travail moyenne
    Alors
        fillRing(2, 255, 255, 255)
        setPixelColor(9, 255, 255, 255)
        AmbianceLumineuse = travail forte
Si AmbianceLumineuse = zen faible
    Alors
        fillRing(1, 218, 251, 102)
        AmbianceLumineuse = zen moyenne
Si AmbianceTravail = travail faible
    Alors
        fillRing(1, 255, 255, 255)
        setPixelColor(9, 255, 255, 255)
        AmbianceLumineuse = travail moyenne

Moins lumineux
--------------
Si AmbianceLumineuse = zen faible, travail faible, fun forte
    Alors
        rien
Si AmbianceLumineuse = zen moyenne
    Alors
        fillRing(1, 0, 0, 0)
        AmbianceLumineuse = zen faible
Si AmbianceLumineuse = travail moyenne
    Alors
        fillRing(1, 0, 0, 0)
        AmbianceLumineuse = travail faible
Si AmbianceLumineuse = zen forte
    Alors
        fillRing(2, 0, 0, 0)
        setPixelColor(9, 0, 0, 0)
        AmbianceLumineuse = zen moyenne
Si AmbianceTravail = travail forte
    Alors
        fillRing(2, 0, 0, 0)
        setPixelColor(9, 0, 0, 0)
        AmbianceLumineuse = travail moyenne

Chant précédent
---------------
music/control/previous

Musique On / Off
----------------
music/control/toggle

Chant suivant
-------------
music/control/next

Volume plus fort
----------------
Si AmbianceMusicale = forte
    Alors
        rien
Si AmbianceMusicale = moyenne
    Alors
        music/control/setvol = 100
        AmbianceMusicale = forte
Si AmbianceMusicale = faible
    Alors
        music/control/setvol = 80
        AmbianceMusicale = moyenne
Si AmbianceMusicale = eteinte
    Alors
        music/control/setvol = 60
        music/control/play
        AmbianceMusicale = faible

Volume moins fort
-----------------
Si AmbianceMusicale = eteinte
    Alors
        rien
Si AmbianceMusicale = forte
    Alors
        music/control/setvol = 80
        AmbianceMusicale = moyenne
Si AmbianceMusicale = moyenne
    Alors
        music/control/setvol = 60
        AmbianceMusicale = faible
Si AmbianceMusicale = faible
    Alors
        music/control/setvol = 0
        music/control/stop
        AmbianceMusicale = eteinte
