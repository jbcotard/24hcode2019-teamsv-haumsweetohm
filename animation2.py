#!/usr/bin/python3
import time
import sys
from laumio import *

# publish.single("laumio/Laumio_104F03/json", "{  'command': 'animate_rainbow' }", hostname="mpd.lan", port=1883)


def start():
    print ("Start Animation.......")

    #// demarrage musique 
    publish.single("music/control/setvol", "90", hostname="mpd.lan", port=1883)
    publish.single("music/control/play", "", hostname="mpd.lan", port=1883)

    laumio1 = Laumio("Laumio_1D9486")
    laumio2 = Laumio("Laumio_104A13")
    laumio3 = Laumio("Laumio_0FBFBF")
    laumio4 = Laumio("Laumio_104F03")
    laumio5 = Laumio("Laumio_10508F")
    laumio6 = Laumio("Laumio_10805F")
    laumio7 = Laumio("Laumio_CD0522")
    laumio8 = Laumio("Laumio_0FC168")
    laumio9 = Laumio("Laumio_D454DB")
    laumio10 = Laumio("Laumio_107DA8")
    laumio11 = Laumio("Laumio_88813D")

    # Tout éteindre

    #laumio/all/fill 0 0 0
    laumio1.wipeOut()
    laumio2.wipeOut()
    laumio3.wipeOut()
    laumio4.wipeOut()
    laumio5.wipeOut()
    laumio6.wipeOut()
    laumio7.wipeOut()
    laumio8.wipeOut()
    laumio9.wipeOut()
    laumio10.wipeOut()
    laumio11.wipeOut()

    #Bel allumage des 3 boules du bas
    #[9, 10]
    #laumio/D454DB/color_wipe 255 0 0 3
    #laumio/107DA8/color_wipe 255 0 0 3

    sleep(2)
    laumio9.colorWipe(255, 0, 0, 1000)
    laumio10.colorWipe(255, 0, 0, 1000)


    #Allumage par anneau pour les boules du milieu
    #[6, 7, 8]
    sleep(2)
    #laumio/0FC168/set_ring 1 0 255 0
    #laumio/CD0522/set_ring 2 0 255 0
    #laumio/10805F/set_ring 3 0 255 0
    laumio6.fillRing(0, 0, 255,0)
    laumio7.fillRing(0, 0, 255,0)
    laumio8.fillRing(0, 0, 255,0)
    sleep(2)
    #laumio/0FC168/set_ring 1 0 126 255
    #laumio/CD0522/set_ring 2 0 126 255
    #laumio/10805F/set_ring 1 0 126 255
    laumio6.fillRing(1, 0, 126, 255)
    laumio7.fillRing(1, 0, 126, 255)
    laumio8.fillRing(1, 0, 126, 255)
    sleep(2)
    #laumio/0FC168/set_ring 1 255 255 0
    #laumio/CD0522/set_ring 2 255 255 0
    #laumio/10805F/set_ring 3 255 255 0
    laumio6.fillRing(2, 255, 255, 0)
    laumio7.fillRing(2, 255, 255, 0)
    laumio8.fillRing(2, 255, 255, 0)



    #Allumage par colonne
    #[1, 3, 4]
    sleep(2)
    #laumio/1D9486/set_column 1 255 0 255
    #laumio/0FBFBF/set_column 1 255 0 255
    #laumio/104F03/set_column 1 255 0 255
    laumio1.fillColumn(0, 255, 0, 255)
    laumio3.fillColumn(0, 255, 0, 255)
    laumio4.fillColumn(0, 255, 0, 255)
    sleep(2)
    #laumio/1D9486/set_column 1 0 255 255
    #laumio/0FBFBF/set_column 1 0 255 255
    #laumio/104F03/set_column 1 0 255 255
    laumio1.fillColumn(1, 0, 255, 255)
    laumio3.fillColumn(1, 0, 255, 255)
    laumio4.fillColumn(1, 0, 255, 255)
    sleep(2)
    #laumio/1D9486/set_column 1 255 255 0
    #laumio/0FBFBF/set_column 1 255 255 0
    #laumio/104F03/set_column 1 255 255 0
    laumio1.fillColumn(2, 255, 255, 0)
    laumio3.fillColumn(2, 255, 255, 0)
    laumio4.fillColumn(2, 255, 255, 0)
    sleep(2)
    #laumio/1D9486/set_column 1 255 0 0
    #laumio/0FBFBF/set_column 1 255 0 0
    #laumio/104F03/set_column 1 255 0 0
    laumio1.fillColumn(3, 255, 0, 0)
    laumio3.fillColumn(3, 255, 0, 0)
    laumio4.fillColumn(3, 255, 0, 0)


    #Traitement pixel par pixel de la dernière boule : une petite
    #lumière bleue qui se balade
    #[11]
    sleep(2)
    #laumio/88813D/set_pixel 0 0 0 255
    laumio11.setPixelColor(0, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 0 0 0 0
    #laumio/88813D/set_pixel 1 0 0 255
    laumio11.setPixelColor(0, 0, 0, 0)
    laumio11.setPixelColor(1, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 1 0 0 0
    #laumio/88813D/set_pixel 2 0 0 255
    laumio11.setPixelColor(1, 0, 0, 0)
    laumio11.setPixelColor(2, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 2 0 0 0
    #laumio/88813D/set_pixel 3 0 0 255
    laumio11.setPixelColor(2, 0, 0, 0)
    laumio11.setPixelColor(3, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 3 0 0 0
    #laumio/88813D/set_pixel 4 0 0 255
    laumio11.setPixelColor(3, 0, 0, 0)
    laumio11.setPixelColor(4, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 4 0 0 0
    #laumio/88813D/set_pixel 5 0 0 255
    laumio11.setPixelColor(4, 0, 0, 0)
    laumio11.setPixelColor(5, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 5 0 0 0
    #laumio/88813D/set_pixel 6 0 0 255
    laumio11.setPixelColor(5, 0, 0, 0)
    laumio11.setPixelColor(6, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 6 0 0 0
    #laumio/88813D/set_pixel 7 0 0 255
    laumio11.setPixelColor(6, 0, 0, 0)
    laumio11.setPixelColor(7, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 7 0 0 0
    #laumio/88813D/set_pixel 8 0 0 255
    laumio11.setPixelColor(7, 0, 0, 0)
    laumio11.setPixelColor(8, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 8 0 0 0
    #laumio/88813D/set_pixel 10 0 0 255
    laumio11.setPixelColor(8, 0, 0, 0)
    laumio11.setPixelColor(10, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 10 0 0 0
    #laumio/88813D/set_pixel 11 0 0 255
    laumio11.setPixelColor(10, 0, 0, 0)
    laumio11.setPixelColor(11, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 11 0 0 0
    #laumio/88813D/set_pixel 12 0 0 255
    laumio11.setPixelColor(11, 0, 0, 0)
    laumio11.setPixelColor(12, 0, 0, 255)
    sleep(1)
    #laumio/88813D/set_pixel 12 0 0 0
    #laumio/88813D/set_pixel 9 0 0 255
    laumio11.setPixelColor(12, 0, 0, 0)
    laumio11.setPixelColor(9, 0, 0, 255)
   
    publish.single("music/control/stop", "", hostname="mpd.lan", port=1883)
    print("End......")

def animation(distance):
    laumio1 = Laumio("Laumio_1D9486")

# MQTT function
def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))
    print(obj)

#
# Reception d'un message
#
def on_message(mqttc, obj, msg):
    print(msg.topic+" / "+str(msg.qos)+" "+str(msg.payload))
    payload = msg.payload.decode("utf-8")

    if msg.topic == 'remote/power/state' and payload == 'ON':
        start()

    if msg.topic == 'distance/value':
        distance = msg.payload.decode("utf-8")
        animation(distance)

def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

if __name__ == '__main__':

    mqttc = mqtt.Client()
    # mqttc.on_message = on_message
    # mqttc.on_connect = on_connect
    # mqttc.on_publish = on_publish
    # mqttc.on_subscribe = on_subscribe
    # Uncomment to enable debug messages


    mqttc.connect("mpd.lan", 1883, 60)

    #mqttc.connect("localhost", 1883, 0)

    mqttc.subscribe("capteur_bp/switch/1/state", 1)
    mqttc.subscribe("capteur_bp/switch/2/state", 1)
    mqttc.subscribe("remote/power/state", 1)

#    mqttc.on_log = on_log
    mqttc.on_message = on_message

    #mqttc.loop_start()

    mqttc.loop_forever()
print("End")
