#/usr/bin/env python3

import time
import paho.mqtt.client as mqtt
import sys
from time import sleep
import paho.mqtt.publish as publish

class Laumio:
    """ Laumio's abstraction layer

    """

    def __init__(self, id):
        self.__id = id

    def wipeOut(self):
        """ Shut down all the LED """
        return self.fillColor(0, 0, 0)

    def fillColor(self, r, g, b):
        """ Set the color of all the leds in the Laumio
        r -- red byte
        g -- green byte
        b -- blue byte
        """
        return publish.single("laumio/"+ self.__id +"/json", "{  'command': 'fill', 'rgb': ["+str(r)+","+str(g) +","+str(b) +"]}", hostname="mpd.lan", port=1883)

    def fillRing(self, ringid, r, g, b):
        """ Set the color of all the leds of ring ringid in the Laumio
        ringid -- Ring ID (0~2)
        r -- red byte
        g -- green byte
        b -- blue byte
        """
        return publish.single("laumio/"+ self.__id +"/json", "{  'command': 'set_ring', 'ring': "+ str(ringid) + ", 'rgb': ["+str(r)+","+str(g) +","+str(b) +"]}", hostname="mpd.lan", port=1883)

    def fillColumn(self, columnid, r, g, b):
        """ Set the color of all the leds of column columnid in the Laumio
        columnid -- Column ID (0~3)
        r -- red byte
        g -- green byte
        b -- blue byte
        """
        return publish.single("laumio/"+ self.__id +"/json", "{  'command': 'set_column', 'column': "+ str(columnid) + ", 'rgb': ["+str(r)+","+str(g) +","+str(b) +"]}", hostname="mpd.lan", port=1883)

    def setPixelColor(self, pixel, r, g, b):
        """ Set the color of all the leds pixel of the Laumio
        pixel -- LED ID (0~12)
        r -- red byte
        g -- green byte
        b -- blue byte
        """
        return publish.single("laumio/"+ self.__id +"/json", "{  'command': 'set_pixel', 'led': "+ str(pixel) + ", 'rgb': ["+str(r)+","+str(g) +","+str(b) +"]}", hostname="mpd.lan", port=1883)

    def colorWipe(self, r, g, b, delay):
        """ Send colorWipe animation with the specified color and delay
        r -- red byte
        g -- green byte
        b -- blue byte
        delay -- delay to wait between LED
        """
        return publish.single("laumio/"+ self.__id +"/json", "{  'command': 'color_wipe', 'duration': "+ str(delay) + ", 'rgb': ["+str(r)+","+str(g) +","+str(b) +"]}", hostname="mpd.lan", port=1883)

    def rainbow(self):
        """ Start Rainbow animation """
        return publish.single("laumio/"+ self.__id +"/json", "{  'command': 'animate_rainbow', }", hostname="mpd.lan", port=1883)


    
